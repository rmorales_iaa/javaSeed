/*
  Java library for miniSeed decompression files. Based on:
    
  https://github.com/crotwell/seedCodec
  https://github.com/crotwell/seisfile
  http://www.seis.sc.edu/seisFile.html
  http://www.seis.sc.edu/seedCodec.html
  https://github.com/iris-edu/msi
 */

//-----------------------------------------------------------------------------
//Package section
//-----------------------------------------------------------------------------
package javaSeed;

//-----------------------------------------------------------------------------
//Import section
//-----------------------------------------------------------------------------
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;


import edu.iris.dmc.seedcodec.UnsupportedCompressionType;
import edu.iris.dmc.seedcodec.CodecException;
import edu.iris.dmc.seedcodec.DecompressedData;

import edu.sc.seis.seisFile.mseed.Btime;
import edu.sc.seis.seisFile.mseed.DataRecord;
import edu.sc.seis.seisFile.mseed.SeedFormatException;
import edu.sc.seis.seisFile.mseed.SeedRecord;

//-----------------------------------------------------------------------------
//Code section
//-----------------------------------------------------------------------------
public class Main {

	//-----------------------------------------------------------------------------
	private static Boolean generateErrorAndClose (String message
	  , Exception e
	  , DataInputStream inputStream
	  , PrintWriter outStream) {
		
		System.out.println(message);
		
		try {
			inputStream.close();
		} catch (IOException e1) {			
			e1.printStackTrace();
		}
		outStream.close();
		e.printStackTrace();
		return false;			
	}

	//-----------------------------------------------------------------------------
	public static Boolean parseMiniSeedFileIntData (String filename
	, ArrayList<Integer> dataResult
	, long [] startTime
	, int [] sampleRate) {
	
		//assuming the same sample rate for all records
		//assuming continous time at sample rate for all records
		
		//open the file
		DataInputStream inputStream = null;
		try {
			inputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(filename),4096)); //4096 is the buffer size			
			
		} catch (FileNotFoundException e1) {
			System.out.println("File not found: "+filename);
			e1.printStackTrace();
			return false;
		}
		PrintWriter outStream = new PrintWriter(System.out, true);
		Boolean moreRecord = true;
		int recordCount = 0;
		int sampleCount = 0;
		
		//parse all records
		while (moreRecord) {
			
			//read the record, checking it if the last one
		    SeedRecord sr = null;
			try {
				sr = SeedRecord.read(inputStream, 4096);
				
			} catch (SeedFormatException e) {		
				return generateErrorAndClose("Error reading a record from: "+filename,e,inputStream,outStream);
			}
			catch (IOException eIO) {
				moreRecord = false;
				continue;
			}
			
			//print the header of the record
			//too much info			
		    /*try {
				sr.writeASCII(outStream);
			} catch (IOException e) {
				return generateErrorAndClose("Error writing a record from: "+filename,e,inputStream,outStream);
			}*/
		    
		    //decompress the record
		    if (sr instanceof DataRecord) {
		    	++recordCount;
		        DataRecord record = (DataRecord)sr;
		        int recordId = record.getHeader().getSequenceNum();		        
		        sampleCount += record.getHeader().getNumSamples();
		       
		        DecompressedData dd = null;
				try {
					dd = record.decompress();
				} catch (SeedFormatException e) {
					return generateErrorAndClose("Error in record format in file: "+filename,e,inputStream,outStream);
				} catch (UnsupportedCompressionType e) {					
					return generateErrorAndClose("Error unssoported compression format in file: "+filename,e,inputStream,outStream);
				} catch (CodecException e) {
					return generateErrorAndClose("Error in coded in file: "+filename,e,inputStream,outStream);
				}
				
				//store the result
				System.out.println("Record " + recordId + " start time: " + record.getStartBtime().convertToCalendar().getTimeInMillis() 
						+ " Start ["+  record.getStartTime() +"]"
						+ " End ["+  record.getEndTime() +"]" 
				        + " Count: [" + record.getHeader().getNumSamples()+"]" );
				int[] array = dd.getAsInt();		    
				for (int k = 0; k < array.length; k++) 
					dataResult.add(array[k]);
				
				
				//get usefull information
				if (recordCount == 1) {
					Btime bTime = record.getStartBtime();					
					startTime[0] = bTime.convertToCalendar().getTimeInMillis();
					sampleRate[0] = Math.round(record.getSampleRate());
				}				
		    }
		}
		
		//check the samples
		int maxSample = dataResult.size();		
		if (sampleCount != maxSample) {		
			System.out.println("Sample mistmatch. Expecting: "+sampleCount+" but found: "+maxSample);
			return false;
		}
		
		//print some stats		
		long endTime = startTime[0] + ((1000 / sampleRate[0]) * sampleCount );
		System.out.println("Parsed file: " + filename);
		System.out.println("Sample rate: " + sampleRate[0]);
		System.out.println("Start time : " + startTime[0]);
		System.out.println("End time   : " + endTime);
		System.out.println("Total parsed record count: "+ recordCount);
		System.out.println("Total parsed sample count: "+ sampleCount);
		System.out.println("First sample value: "+ dataResult.get(0));
		System.out.println("Last  sample value: "+ dataResult.get(maxSample -1));
		
		return true;
	}
	
	//-----------------------------------------------------------------------------
	private static void writeValueList(String fileName
	, ArrayList<Integer> list
	, long startTime
	, int sampleRate
	, long startTimeFilter
	, long endTimeFilter) 
			throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		 
		DateTimeFormatter shortFormat = DateTimeFormatter.ofPattern("HH:mm:ss").withZone( ZoneId.of("UTC") );
		  
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
			new FileOutputStream(fileName), "utf-8"))) {
				long sampleRateMS = 1000 / sampleRate;
				long k = 0;
				long t = 0;				
				for(Integer v: list){
						
					t = startTime + (k * sampleRateMS);
					ZonedDateTime zt = ZonedDateTime.ofInstant(Instant.ofEpochMilli( t ), ZoneId.of("UTC") );
					String longDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format( zt );
					String shortDate = shortFormat.format( zt );
					if ((t >= startTimeFilter) && 
					    (t <= endTimeFilter))
						 writer.write(t+","
									  +longDate+","
									  +shortDate+","
						  		      +v+"\n");
						++k;
					}
			}
	}
	
	//-----------------------------------------------------------------------------
	
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		
		String filenameMin = "/media/data2TB/vane/seismic/caleta_cierva_station/2008/CCV08061/80611151.L-Z"; 
			
		ArrayList<Integer> list = new ArrayList<Integer>();
		long [] startTime = {-1};
		int [] sampleRate = {-1};
		if (parseMiniSeedFileIntData(filenameMin,list, startTime, sampleRate)) {
			System.out.println("Sucess!!");
		}
	
		//write the values in a file	
		writeValueList("out.csv"
			, list
			, startTime[0]
			, sampleRate[0]		
			, 1204372302106L    //startTimeFilter
			, 1204376416926L);  //endTimeFilter
	}

	//-----------------------------------------------------------------------------	
}
